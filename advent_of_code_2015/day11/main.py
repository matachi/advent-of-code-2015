#!/usr/bin/env python3

puzzle_input = 'cqjxjnds'


def increment_string(string):
    """
    >>> increment_string('cqjxjnds')
    'cqjxjndt'
    >>> increment_string('cqjxzzz')
    'cqjyaaa'
    """
    for i in reversed(range(len(string))):
        letter = string[i]
        next_letter = chr(ord(letter) + 1)
        if not next_letter.isalpha():
            next_letter = 'a'
        string = string[:i] + next_letter + string[i+1:]
        if next_letter != 'a':
            return string


def contains_three_letter_sequence(string):
    for i in range(len(string) - 2):
        if ord(string[i]) == ord(string[i + 1]) - 1 and \
                ord(string[i]) == ord(string[i + 2]) - 2:
            return True
    return False


def contains_valid_letters(string):
    return len(set(string).intersection({'i', 'o', 'l'})) == 0


def contains_two_letter_pairs(string):
    pairs = 0
    i = 0
    while i < len(string) - 1:
        if string[i] == string[i + 1]:
            pairs += 1
            i += 2
        else:
            i += 1
    return pairs >= 2


def is_valid_password(string):
    """
    >>> is_valid_password('hijklmmn')
    False
    >>> is_valid_password('abbceffg')
    False
    >>> is_valid_password('abbcegjk')
    False
    >>> is_valid_password('abcdffaa')
    True
    >>> is_valid_password('ghjaabcc')
    True
    """
    return contains_three_letter_sequence(string) and \
        contains_valid_letters(string) and \
        contains_two_letter_pairs(string)


def recursive_increment(value, function):
    while True:
        value = function(value)
        yield value


def get_next_password(old_password):
    """
    >>> get_next_password('abcdefgh')
    'abcdffaa'
    """
    return next(
        password for password in recursive_increment(
            old_password, increment_string)
        if is_valid_password(password))


def part1():
    return get_next_password(puzzle_input)


def part2():
    return get_next_password(part1())


if __name__ == '__main__':
    print(part1())
    print(part2())
