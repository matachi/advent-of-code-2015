#!/usr/bin/env python3
import copy
import os
from itertools import product

path = os.path.join(os.path.dirname(__file__), 'input')


def get_map():
    yield [False] * 102
    with open(path) as f:
        for row in f.readlines():
            yield [False] + [p == '#' for p in row.strip()] + [False]
    yield [False] * 102


def iterate(map_, iterations):
    """
    >>> for row in iterate([
    ...     [False, False, False, False, False, False, False, False],
    ...     [False, False, True, False, True, False, True, False],
    ...     [False, False, False, False, True, True, False, False],
    ...     [False, True, False, False, False, False, True, False],
    ...     [False, False, False, True, False, False, False, False],
    ...     [False, True, False, True, False, False, True, False],
    ...     [False, True, True, True, True, False, False, False],
    ...     [False, False, False, False, False, False, False, False],
    ... ], 1):
    ...     print(row)
    [False, False, False, False, False, False, False, False]
    [False, False, False, True, True, False, False, False]
    [False, False, False, True, True, False, True, False]
    [False, False, False, False, True, True, False, False]
    [False, False, False, False, False, False, False, False]
    [False, True, False, False, False, False, False, False]
    [False, True, False, True, True, False, False, False]
    [False, False, False, False, False, False, False, False]
    """
    if iterations == 0:
        return map_
    new_map = copy.deepcopy(map_)
    for x, y in product(range(1, len(map_) - 1), repeat=2):
        neighbors = map_[x - 1][y - 1] + map_[x][y - 1] + \
            map_[x + 1][y - 1] + map_[x + 1][y] + map_[x + 1][y + 1] + \
            map_[x][y + 1] + map_[x - 1][y + 1] + map_[x - 1][y]
        if map_[x][y] and neighbors not in [2, 3]:
            new_map[x][y] = False
        elif not map_[x][y] and neighbors == 3:
            new_map[x][y] = True
    return iterate(new_map, iterations - 1)


def part1():
    return sum(sum(row) for row in iterate(list(get_map()), 100))


def iterate2(map_, iterations):
    if iterations == 0:
        return map_
    new_map = copy.deepcopy(map_)
    for x, y in product(range(1, len(map_) - 1), repeat=2):
        neighbors = map_[x - 1][y - 1] + map_[x][y - 1] + \
            map_[x + 1][y - 1] + map_[x + 1][y] + map_[x + 1][y + 1] + \
            map_[x][y + 1] + map_[x - 1][y + 1] + map_[x - 1][y]
        if map_[x][y] and neighbors not in [2, 3]:
            new_map[x][y] = False
        elif not map_[x][y] and neighbors == 3:
            new_map[x][y] = True
    new_map[1][1] = True
    new_map[len(map_) - 2][1] = True
    new_map[1][len(map_) - 2] = True
    new_map[len(map_) - 2][len(map_) - 2] = True
    return iterate2(new_map, iterations - 1)


def part2():
    map_ = list(get_map())
    map_[1][1] = True
    map_[100][1] = True
    map_[1][100] = True
    map_[100][100] = True
    return sum(sum(row) for row in iterate2(map_, 100))


if __name__ == '__main__':
    print(part1())
    print(part2())
