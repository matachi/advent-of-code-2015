#!/usr/bin/env python3
import os
from itertools import combinations
from operator import mul, add

path = os.path.join(os.path.dirname(__file__), 'input')


def get_measurements():
    """
    :return: A list of tuples, where each tuple is the width, height and
        length.
    :rtype: list[tuple[int, int, int]]
    """
    with open(path) as f:
        return [
            tuple(map(int, l.strip().split('x')))
            for l in f.readlines()]


def part1():
    measurements = get_measurements()
    # For each measurement:
    # - Make a list of all combination of the numbers, i.e. [(w, h), (w, l),
    #   (l, h)].
    # - For each combination, multiply the numbers with each other in that
    #   pair. I.e. [w*h, w*l, l*h].
    sides_sizes = [
        list(map(lambda x: mul(*x), combinations(m, 2)))
        for m in measurements]
    # For each package, sum the 3 sides and multiply with 2 to get the size of
    # all 6 sides.
    wrapping_paper = 2 * sum(map(sum, sides_sizes))
    # The slack is the smallest side of each package.
    slack = sum(map(min, sides_sizes))
    print(wrapping_paper + slack)


def part2():
    measurements = get_measurements()
    # For each measurement, add the two shortest sides together and multiply
    # them with 2.
    wrap = sum(2 * add(*sorted(m)[:-1]) for m in measurements)
    # The bow is simply w*h*l for each measurement.
    bow = sum(w * h * l for w, h, l in measurements)
    print(wrap + bow)


if __name__ == '__main__':
    part1()
    part2()
