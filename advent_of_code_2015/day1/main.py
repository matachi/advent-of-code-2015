#!/usr/bin/env python3
import os
from itertools import accumulate

path = os.path.join(os.path.dirname(__file__), 'input')


def part1():
    with open(path) as f:
        print(sum(1 if char == '(' else -1 for char in f.read()))


def part2():
    with open(path) as f:
        steps = (1 if char == '(' else -1 for char in f.read())
        print(next(
            i for i, level in enumerate(accumulate(steps), 1) if level == -1))


if __name__ == '__main__':
    part1()
    part2()
