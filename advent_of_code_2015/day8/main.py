#!/usr/bin/env python3
import os

path = os.path.join(os.path.dirname(__file__), 'input')


def get_lines():
    with open(path) as f:
        return [line.strip() for line in f.readlines()]


def part1():
    lines = get_lines()
    code_length = sum(map(len, lines))
    # http://stackoverflow.com/a/4020824/595990
    memory_strings = [
        bytes(line[1:-1], 'utf-8').decode('unicode_escape')
        for line in lines]
    memory_length = sum(map(len, memory_strings))
    return code_length - memory_length


def escaped_string_length(string):
    """
    >>> escaped_string_length('""')
    6
    >>> escaped_string_length('"abc"')
    9
    >>> escaped_string_length(r'"aaa\\"aaa"')
    16
    >>> escaped_string_length(r'"\\x27"')
    11
    """
    return len(string) + len([l for l in string if l in ['"', '\\']]) + 2


def part2():
    lines = get_lines()
    code_length = sum(map(len, lines))
    escaped_length = sum(map(escaped_string_length, lines))
    return escaped_length - code_length


if __name__ == '__main__':
    print(part1())
    print(part2())
