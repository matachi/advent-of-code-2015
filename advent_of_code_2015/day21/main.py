#!/usr/bin/env python3
import os
from itertools import cycle, combinations

path = os.path.join(os.path.dirname(__file__), 'input')


def get_boss_stats():
    with open(path) as f:
        rows = f.readlines()
        hit_points = int(rows[0].split()[-1])
        damage = int(rows[1].split()[-1])
        armor = int(rows[2].split()[-1])
        return hit_points, damage, armor


def shop_items():
    return {
        'weapons': {
            # Cost - damage - armor
            'Dagger': (8, 4, 0),
            'Shortsword': (10, 5, 0),
            'Warhammer': (25, 6, 0),
            'Longsword': (40, 7, 0),
            'Greataxe': (74, 8, 0),
        }, 'armors': {
            # Cost - damage - armor
            'Leather': (13, 0, 1),
            'Chainmail': (31, 0, 2),
            'Splintmail': (53, 0, 3),
            'Bandedmail': (75, 0, 4),
            'Platemail': (102, 0, 5),
        }, 'rings': {
            # Cost - damage - armor
            'Damage +1': (25, 1, 0),
            'Damage +2': (50, 2, 0),
            'Damage +3': (100, 3, 0),
            'Defense +1': (20, 0, 1),
            'Defense +2': (40, 0, 2),
            'Defense +3': (80, 0, 3),
        }
    }


def simulate(boss_stats, items):
    health = 100
    boss_health = boss_stats[0]
    damage = sum(item[1] for item in items)
    armor = sum(item[2] for item in items)
    for player_turn in cycle([True, False]):
        if player_turn:
            boss_health -= max(damage - boss_stats[2], 1)
            if boss_health <= 0:
                return True
        else:
            health -= max(boss_stats[1] - armor, 1)
            if health <= 0:
                return False


def item_combinations(items):
    for weapon in items['weapons'].values():
        for armor in list(items['armors'].values()) + [None]:
            for ring1, ring2 in combinations(
                    list(items['rings'].values()) + [None, None], 2):
                selected_items = [weapon]
                if armor is not None:
                    selected_items.append(armor)
                if ring1 is not None:
                    selected_items.append(ring1)
                if ring2 is not None:
                    selected_items.append(ring2)
                yield selected_items


def part1():
    boss_stats = get_boss_stats()
    return min(
        sum(item[0] for item in item_combination)
        for item_combination in item_combinations(shop_items())
        if simulate(boss_stats, item_combination))


def part2():
    boss_stats = get_boss_stats()
    return max(
        sum(item[0] for item in item_combination)
        for item_combination in item_combinations(shop_items())
        if not simulate(boss_stats, item_combination))


if __name__ == '__main__':
    print(part1())
    print(part2())
