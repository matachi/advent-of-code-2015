#!/usr/bin/env python3
import os
import re

path = os.path.join(os.path.dirname(__file__), 'input')


def get_strings():
    with open(path) as f:
        return [line.strip() for line in f.readlines()]


vowels_pattern = re.compile(r'[aeiou]')
two_letters_pattern = re.compile(r'(\w)\1')
disallowed_sequences_pattern = re.compile(r'ab|cd|pq|xy')


def is_string_nice(string):
    """
    >>> is_string_nice('ugknbfddgicrmopn')
    True
    >>> is_string_nice('aaa')
    True
    >>> is_string_nice('jchzalrnumimnmhp')
    False
    >>> is_string_nice('haegwjzuvuyypxyu')
    False
    >>> is_string_nice('dvszwmarrgswjxmb')
    False

    :type string: str
    :rtype: bool
    """
    if len(vowels_pattern.findall(string)) < 3:
        return False
    if two_letters_pattern.search(string) is None:
        return False
    if disallowed_sequences_pattern.search(string) is not None:
        return False
    return True


def part1():
    return len(list(filter(is_string_nice, get_strings())))


pair_of_two_letters_pattern = re.compile(r'(\w\w).*\1')
repeat_one_letter_between_pattern = re.compile(r'(\w).\1')


def is_string_nice2(string):
    """
    >>> is_string_nice2('qjhvhtzxzqqjkmpb')
    True
    >>> is_string_nice2('xxyxx')
    True
    >>> is_string_nice2('uurcxstgmygtbstg')
    False
    >>> is_string_nice2('ieodomkazucvgmuy')
    False

    :type string: str
    :rtype: bool
    """
    if pair_of_two_letters_pattern.search(string) is None:
        return False
    if repeat_one_letter_between_pattern.search(string) is None:
        return False
    return True


def part2():
    return len(list(filter(is_string_nice2, get_strings())))


if __name__ == '__main__':
    print(part1())
    print(part2())
