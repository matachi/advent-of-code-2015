#!/usr/bin/env python3
import hashlib
from itertools import count

secret_key = 'bgvyzdsv'


def compute_hash(string):
    return hashlib.md5(string.encode('utf-8')).hexdigest()


def part1():
    return next(
        i for i in count()
        if compute_hash('%s%i' % (secret_key, i)).startswith('00000'))


def part2():
    return next(
        i for i in count()
        if compute_hash('%s%i' % (secret_key, i)).startswith('000000'))


if __name__ == '__main__':
    print(part1())
    print(part2())
