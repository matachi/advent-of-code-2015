#!/usr/bin/env python3
import json
import os
import re

path = os.path.join(os.path.dirname(__file__), 'input')


def get_file_contents():
    with open(path) as f:
        return f.read()


def part1():
    return sum(map(int, re.compile('-?\d+').findall(get_file_contents())))


def sum_in_obj(obj):
    if isinstance(obj, int):
        return obj
    elif isinstance(obj, str):
        return 0
    elif isinstance(obj, dict):
        if 'red' in obj.values():
            return 0
        return sum(map(sum_in_obj, obj.keys())) + \
            sum(map(sum_in_obj, obj.values()))
    elif isinstance(obj, list):
        return sum(map(sum_in_obj, obj))
    else:
        raise ValueError(obj)


def part2():
    return sum_in_obj(json.loads(get_file_contents()))


if __name__ == '__main__':
    print(part1())
    print(part2())
