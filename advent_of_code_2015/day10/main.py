#!/usr/bin/env python3

puzzle_input = '1113122113'


def look_and_say_sequence(input_):
    """
    >>> look_and_say_sequence('1')
    '11'
    >>> look_and_say_sequence('11')
    '21'
    >>> look_and_say_sequence('1211')
    '111221'
    >>> look_and_say_sequence('111221')
    '312211'
    """
    input_ += 'a'  # Sentinel value
    new_string = ''
    last_char = input_[0]
    count = 0
    while len(input_) > 0:
        char = input_[0]
        if char == last_char:
            count += 1
        else:
            new_string += '%d%s' % (count, last_char)
            count = 1
            last_char = char
        input_ = input_[1:]
    return new_string


def iterate(input_, iterations):
    if iterations == 0:
        return input_
    else:
        return iterate(look_and_say_sequence(input_), iterations - 1)


def part1():
    return len(iterate(puzzle_input, 40))


def part2():
    return len(iterate(puzzle_input, 50))


if __name__ == '__main__':
    print(part1())
    print(part2())
