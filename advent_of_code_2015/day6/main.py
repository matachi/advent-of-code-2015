#!/usr/bin/env python3
import os
from itertools import product

path = os.path.join(os.path.dirname(__file__), 'input')


def get_instructions():
    with open(path) as f:
        for line in f.readlines():
            parts = line.split()
            func_name = ' '.join(parts[:-3])
            corner1 = tuple(map(int, parts[-3].split(',')))
            corner2 = tuple(map(int, parts[-1].split(',')))
            yield func_name, corner1, corner2


def part1():
    functions = {
        'turn on': lambda bool_: True,
        'turn off': lambda bool_: False,
        'toggle': lambda bool_: not bool_,
    }
    lights = {(x, y): False for x, y in product(range(1000), range(1000))}
    for func_name, (x1, y1), (x2, y2) in get_instructions():
        function = functions[func_name]
        for coordinate in product(range(x1, x2 + 1), range(y1, y2 + 1)):
            lights[coordinate] = function(lights[coordinate])
    return sum(lights.values())


def part2():
    functions = {
        'turn on': lambda int_: int_ + 1,
        'turn off': lambda int_: max(int_ - 1, 0),
        'toggle': lambda int_: int_ + 2,
    }
    lights = {(x, y): 0 for x, y in product(range(1000), range(1000))}
    for func_name, (x1, y1), (x2, y2) in get_instructions():
        function = functions[func_name]
        for coordinate in product(range(x1, x2 + 1), range(y1, y2 + 1)):
            lights[coordinate] = function(lights[coordinate])
    return sum(lights.values())


if __name__ == '__main__':
    print(part1())
    print(part2())
