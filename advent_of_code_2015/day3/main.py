#!/usr/bin/env python3
import os
from collections import defaultdict

path = os.path.join(os.path.dirname(__file__), 'input')


def get_directions():
    with open(path) as f:
        return f.read()


def part1():
    directions = get_directions()
    visited_coordinates = defaultdict(int)
    coordinate = (0, 0)
    visited_coordinates[coordinate] += 1
    for direction in directions:
        x, y = coordinate
        if direction == '<':
            coordinate = (x - 1, y)
        elif direction == '>':
            coordinate = (x + 1, y)
        elif direction == 'v':
            coordinate = (x, y - 1)
        elif direction == '^':
            coordinate = (x, y + 1)
        visited_coordinates[coordinate] += 1
    return len(visited_coordinates)


def get_vectors():
    """
    :return: The movement vectors.
    :rtype: generator[tuple[int, int]]
    """
    with open(path) as f:
        for direction in f.read():
            if direction == '<':
                yield (-1, 0)
            elif direction == '>':
                yield (1, 0)
            elif direction == 'v':
                yield (0, -1)
            elif direction == '^':
                yield (0, 1)


def deliver_packages(vectors):
    # Start in the center
    coordinate = (0, 0)
    yield coordinate
    # For each movement vector, compute the new coordinate and yield it
    for vector in vectors:
        x1, y1 = coordinate
        x2, y2 = vector
        coordinate = x1 + x2, y1 + y2
        yield coordinate


def part2():
    # A list of all movement vectors
    vectors = list(get_vectors())
    # Send every even index to santa, and every odd index to robo-santa. Make
    # a set of all visited coordinates (i.e. the unique houses we have
    # visited). Return the number of unique houses.
    return len(set(
        list(deliver_packages(vectors[0::2])) +
        list(deliver_packages(vectors[1::2]))
    ))


if __name__ == '__main__':
    print(part1())
    print(part2())
