#!/usr/bin/env python3
import os
from functools import reduce
from operator import add

path = os.path.join(os.path.dirname(__file__), 'input')


def get_cell():
    with open(path) as f:
        parts = f.readline().split()
        column, row = int(parts[-1][:-1]), int(parts[-3][:-1])
        return column, row


def part1():
    column, row = get_cell()
    diagonal = column + row - 1
    index_on_diagonal = column
    index = index_on_diagonal + reduce(add, range(1, diagonal))
    val = 20151125
    for _ in range(index - 1):
        val = (val * 252533) % 33554393
    return val


if __name__ == '__main__':
    print(part1())
