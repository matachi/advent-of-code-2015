#!/usr/bin/env python3
import os

correct_sue_properties = {
    'children': 3,
    'cats': 7,
    'samoyeds': 2,
    'pomeranians': 3,
    'akitas': 0,
    'vizslas': 0,
    'goldfish': 5,
    'trees': 3,
    'cars': 2,
    'perfumes': 1,
}


path = os.path.join(os.path.dirname(__file__), 'input')


def get_sues():
    with open(path) as f:
        for row in f.readlines():
            number = int(row.split(':')[0].split()[1])
            properties = dict(
                p.split(': ') for p
                in ':'.join(row.split(':')[1:]).strip().split(', '))
            properties = {key: int(value) for key, value in properties.items()}
            yield number, properties


def get_correct_sue(all_sues, correct_sue):
    for number, properties in all_sues:
        for property, value in correct_sue.items():
            if property in properties and properties[property] != value:
                break
        else:
            return number
    return -1


def part1():
    return get_correct_sue(get_sues(), correct_sue_properties)


def get_correct_sues(all_sues, correct_sue):
    for number, properties in all_sues:
        for property, value in correct_sue.items():
            if property not in properties:
                continue
            elif property == 'cats' or property == 'trees':
                if properties[property] > value:
                    continue
                else:
                    break
            elif property == 'pomeranians' or property == 'goldfish':
                if properties[property] < value:
                    continue
                else:
                    break
            elif properties[property] != value:
                break
        else:
            return number


def part2():
    return get_correct_sues(get_sues(), correct_sue_properties)


if __name__ == '__main__':
    print(part1())
    print(part2())
