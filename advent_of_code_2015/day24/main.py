#!/usr/bin/env python3
import os
from itertools import accumulate
from operator import mul

path = os.path.join(os.path.dirname(__file__), 'input')


def get_present_sizes():
    with open(path) as f:
        return list(map(int, f.readlines()))


def get_groups(presents, size, i=0):
    if i >= len(presents):
        return []
    present = presents[i]
    if present == size:
        return [[present]]
    if present > size:
        return []
    groups = []
    groups.extend(get_groups(presents, size, i + 1))
    groups.extend(
        [[present] + group for group in get_groups(presents, size - present, i + 1)])
    return groups


def fits(presents, size, group):
    if size == 0:
        return True
    if size < 0:
        return False
    if len(presents) == 0:
        return False
    present = presents.pop()
    if fits(list(presents), size, group):
        return True
    elif fits(list(presents), size - present, group + [present]):
        return True
    else:
        return False


def get_best_score(presents, groups):
    group_size = sum(presents) / groups
    groups = get_groups(presents, group_size)
    for size in sorted(set(map(len, groups))):
        for group in sorted(
            filter(lambda g: len(g) == size, groups),
            key=lambda g: list(accumulate(g, mul))[-1]
        ):
            if fits(list(set(presents) - set(group)), group_size, []):
                return list(accumulate(group, mul))[-1]


def part1():
    return get_best_score(get_present_sizes(), 3)


def part2():
    return get_best_score(get_present_sizes(), 4)


if __name__ == '__main__':
    print(part1())
    print(part2())
