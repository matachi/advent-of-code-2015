#!/usr/bin/env python3
import os
from itertools import permutations, chain

path = os.path.join(os.path.dirname(__file__), 'input')


def get_distances():
    with open(path) as f:
        for line in f.readlines():
            city1, _, city2, _, distance = line.split()
            yield city1, city2, int(distance)


def get_path_lengths():
    distances = {
        tuple(sorted([city1, city2])): distance
        for city1, city2, distance in get_distances()}
    cities = set(chain.from_iterable(distances.keys()))
    paths = permutations(cities, len(cities))
    return [
        sum(distances[tuple(sorted([city1, city2]))]
            for city1, city2 in zip(path[:-1], path[1:]))
        for path in paths]


def part1():
    return min(get_path_lengths())


def part2():
    return max(get_path_lengths())


if __name__ == '__main__':
    print(part1())
    print(part2())
