#!/usr/bin/env python3
import os
from itertools import chain

path = os.path.join(os.path.dirname(__file__), 'input')


class Operator:
    def __init__(self, source, target):
        self.source = source
        self.target = target


class Binary(Operator):
    def __call__(self, a, b):
        raise NotImplementedError


class Unary(Operator):
    def __call__(self, a):
        raise NotImplementedError


class And(Binary):
    def __call__(self, a, b):
        return a & b


class Or(Binary):
    def __call__(self, a, b):
        return a | b


class RightShift(Unary):
    def __init__(self, shift_by, source, target):
        super().__init__(source, target)
        self.shift_by = shift_by

    def __call__(self, a):
        return a >> self.shift_by


class LeftShift(Unary):
    def __init__(self, shift_by, source, target):
        super().__init__(source, target)
        self.shift_by = shift_by

    def __call__(self, a):
        return a << self.shift_by


class Not(Unary):
    def __call__(self, a):
        return ~a


class Forward(Unary):
    def __call__(self, a):
        return a


class Constant(Operator):
    def __init__(self, constant, target):
        super().__init__(None, target)
        self.constant = constant

    def __call__(self):
        return self.constant


def get_operators():
    with open(path) as f:
        for line in f.readlines():
            function, target = line.strip().split(' -> ')
            parts = function.split()
            if 'AND' in function:
                yield And([parts[0], parts[2]], target)
            elif 'OR' in function:
                yield Or([parts[0], parts[2]], target)
            elif 'RSHIFT' in function:
                yield RightShift(int(parts[2]), parts[0], target)
            elif 'LSHIFT' in function:
                yield LeftShift(int(parts[2]), parts[0], target)
            elif 'NOT' in function:
                yield Not(parts[1], target)
            elif parts[0].isdecimal():
                yield Constant(int(parts[0]), target)
            else:
                yield Forward(parts[0], target)


def part1():
    operators = list(get_operators())
    operands = [o.target for o in operators]
    operands.extend(
        [o.source for o in operators if isinstance(o, Unary)])
    operands.extend(
        list(chain(*[o.source for o in operators if isinstance(o, Binary)])))
    operands = {o: None for o in operands}
    while operands['a'] is None:
        for operator in list(operators):
            if isinstance(operator, Binary):
                source1 = operator.source[0]
                source2 = operator.source[1]
                if source1.isdecimal():
                    operand1 = int(source1)
                else:
                    operand1 = operands[operator.source[0]]
                if source2.isdecimal():
                    operand2 = int(source1)
                else:
                    operand2 = operands[operator.source[1]]
                if operand1 is None:
                    continue
                if operand2 is None:
                    continue
                result = operator(operand1, operand2)
            elif isinstance(operator, Unary):
                operand = operands[operator.source]
                if operand is None:
                    continue
                result = operator(operand)
            elif isinstance(operator, Constant):
                result = operator()
            else:
                raise ValueError
            if operator.target not in operands:
                raise ValueError
            operands[operator.target] = 0xffff & result
            operators.remove(operator)
    return operands['a']


def part2():
    operators = list(get_operators())
    operands = [o.target for o in operators]
    operands.extend(
        [o.source for o in operators if isinstance(o, Unary)])
    operands.extend(
        list(chain(*[o.source for o in operators if isinstance(o, Binary)])))
    operands = {o: None for o in operands}
    operands['b'] = 956
    while operands['a'] is None:
        for operator in list(operators):
            if operator.target == 'b':
                continue
            if isinstance(operator, Binary):
                source1 = operator.source[0]
                source2 = operator.source[1]
                if source1.isdecimal():
                    operand1 = int(source1)
                else:
                    operand1 = operands[operator.source[0]]
                if source2.isdecimal():
                    operand2 = int(source1)
                else:
                    operand2 = operands[operator.source[1]]
                if operand1 is None:
                    continue
                if operand2 is None:
                    continue
                result = operator(operand1, operand2)
            elif isinstance(operator, Unary):
                operand = operands[operator.source]
                if operand is None:
                    continue
                result = operator(operand)
            elif isinstance(operator, Constant):
                result = operator()
            else:
                raise ValueError
            if operator.target not in operands:
                raise ValueError
            operands[operator.target] = 0xffff & result
            operators.remove(operator)
    return operands['a']


if __name__ == '__main__':
    print(part1())
    print(part2())
