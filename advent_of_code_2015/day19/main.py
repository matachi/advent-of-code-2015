#!/usr/bin/env python3
import os

path = os.path.join(os.path.dirname(__file__), 'input')


def get_molecule_and_transformations():
    with open(path) as f:
        rows = f.readlines()
        molecule = rows[-1].strip()
        return molecule, [row.strip().split(' => ') for row in rows[:-2]]


def get_molecules(molecule, transformations):
    for transformation in transformations:
        i = -1
        while True:
            i = molecule.find(transformation[0], i + 1)
            if i == -1:
                break
            yield molecule[:i] + transformation[1] + \
                molecule[i + len(transformation[0]):]


def part1():
    molecule, transformations = get_molecule_and_transformations()
    return len(set(get_molecules(molecule, transformations)))


def steps_to_find_medicine_molecule(molecule, transformations, steps=0):
    """
    >>> steps_to_find_medicine_molecule('HOHOHO', [
    ...     ('H', 'HO'),
    ...     ('H', 'OH'),
    ...     ('O', 'HH'),
    ...     ('e', 'H'),
    ...     ('e', 'O'),
    ... ])
    6
    """
    if molecule == 'e':
        return steps
    for transformation in transformations:
        i = -1
        while True:
            i = molecule.find(transformation[1], i + 1)
            if i == -1:
                break
            new_molecule = molecule[:i] + transformation[0] + \
                molecule[i + len(transformation[1]):]
            steps_ = steps_to_find_medicine_molecule(
                new_molecule, transformations, steps + 1)
            if steps_ != -1:
                return steps_
    return -1


def part2():
    molecule, transformations = get_molecule_and_transformations()
    transformations.sort(key=lambda x: len(x[1]), reverse=True)
    return steps_to_find_medicine_molecule(molecule, transformations)


if __name__ == '__main__':
    print(part1())
    print(part2())
