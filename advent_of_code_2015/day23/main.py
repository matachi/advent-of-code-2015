#!/usr/bin/env python3
import os

path = os.path.join(os.path.dirname(__file__), 'input')


def get_instructions():
    with open(path) as f:
        for row in f.readlines():
            parts = row.split()
            instruction = parts[0]
            if instruction in ['hlf', 'tpl', 'inc']:
                register = parts[1]
                offset = None
            elif instruction in ['jmp']:
                register = None
                offset = int(parts[1])
            elif instruction in ['jio', 'jie']:
                register = parts[1][:1]
                offset = int(parts[2])
            else:
                raise ValueError
            yield instruction, register, offset


def run_program(registers, instructions):
    pc = 0
    while 0 <= pc < len(instructions):
        instruction, register, offset = instructions[pc]
        if instruction == 'hlf':
            registers[register] /= 2
            pc += 1
        elif instruction == 'tpl':
            registers[register] *= 3
            pc += 1
        elif instruction == 'inc':
            registers[register] += 1
            pc += 1
        elif instruction == 'jmp':
            pc += offset
        elif instruction == 'jie':
            if registers[register] % 2 == 0:
                pc += offset
            else:
                pc += 1
        elif instruction == 'jio':
            if registers[register] == 1:
                pc += offset
            else:
                pc += 1
        else:
            raise ValueError
    return registers['b']


def part1():
    registers = {'a': 0, 'b': 0}
    return run_program(registers, list(get_instructions()))


def part2():
    registers = {'a': 1, 'b': 0}
    return run_program(registers, list(get_instructions()))


if __name__ == '__main__':
    print(part1())
    print(part2())
