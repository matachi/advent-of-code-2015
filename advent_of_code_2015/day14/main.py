#!/usr/bin/env python3
import os

path = os.path.join(os.path.dirname(__file__), 'input')


class Reindeer:
    def __init__(self, velocity, fly, rest):
        self.velocity = velocity
        self.fly = fly
        self.rest = rest

    def race(self, end_after):
        is_flying = True
        time = 0
        activity_time = 0
        distance = 0
        while time < end_after:
            if (is_flying and activity_time == self.fly) or \
                    (not is_flying and activity_time == self.rest):
                is_flying = not is_flying
                activity_time = 0
            if is_flying:
                distance += self.velocity
            time += 1
            activity_time += 1
        return distance


def get_reindeers():
    with open(path) as f:
        for row in f.readlines():
            parts = row.split()
            yield Reindeer(int(parts[3]), int(parts[6]), int(parts[-2]))


def part1():
    return max(reindeer.race(2503) for reindeer in get_reindeers())


def part2():
    reindeers = list(get_reindeers())
    scores = [0] * len(reindeers)
    for i in range(1, 2504):
        distances = list(
            map(lambda j: reindeers[j].race(i), range(len(reindeers))))
        leader_distance = max(distances)
        for k, distance in enumerate(distances):
            if distance == leader_distance:
                scores[k] += 1
    return max(scores)


if __name__ == '__main__':
    print(part1())
    print(part2())
