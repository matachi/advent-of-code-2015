#!/usr/bin/env python3


input_ = 33100000


def first_house_with_presents(target_presents):
    house = 1
    while True:
        presents_at_this_house = sum(
            [10 * i for i in range(1, house + 1) if house % i == 0])
        if presents_at_this_house >= target_presents:
            return house
        else:
            house += 1


def first_house_with_presents2(target_presents):
    target_presents //= 10
    elves = target_presents // 2
    houses = [0] * elves
    best_house = 9999999999
    for elf in range(1, elves):
        for house in range(elf, elves, elf):
            houses[house] += elf
            if houses[house] > target_presents:
                if house < best_house:
                    best_house = house
                    print(best_house)
                continue
    return best_house


def part1():
    return first_house_with_presents2(input_)


def first_house_with_presents3(target_presents):
    elves = target_presents // 2
    houses = [0] * elves
    best_house = 9999999999
    for elf in range(1, elves):
        for house in range(elf, 50 * elf, elf):
            if house >= len(houses):
                break
            houses[house] += 11 * elf
            if houses[house] > target_presents:
                if house < best_house:
                    best_house = house
                    print(best_house)
                continue
    return best_house


def part2():
    return first_house_with_presents3(input_)


if __name__ == '__main__':
    print(part1())
    print(part2())
