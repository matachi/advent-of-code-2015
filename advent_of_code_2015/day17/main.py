#!/usr/bin/env python3
import os
from itertools import chain, combinations


path = os.path.join(os.path.dirname(__file__), 'input')


def get_bottles():
    with open(path) as f:
        return [int(size) for size in f.readlines()]


def part1():
    bottles = get_bottles()
    return len(list(filter(
        lambda x: sum(x) == 150,
        chain.from_iterable(
            combinations(bottles, i)
            for i in range(1, len(bottles) + 1)
        )
    )))


def part2():
    bottles = get_bottles()
    for i in range(1, len(bottles) + 1):
        bottle_combinations = len(list(filter(
            lambda x: sum(x) == 150,
            combinations(bottles, i)
        )))
        if bottle_combinations > 0:
            return bottle_combinations


if __name__ == '__main__':
    print(part1())
    print(part2())
