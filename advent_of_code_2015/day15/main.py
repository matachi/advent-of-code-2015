#!/usr/bin/env python3
import os
from functools import partial
from itertools import accumulate
from operator import mul

path = os.path.join(os.path.dirname(__file__), 'input')


def get_ingredients():
    with open(path) as f:
        for row in f.readlines():
            parts = row.split()
            yield int(parts[2][:-1]), int(parts[4][:-1]), int(parts[6][:-1]), \
                int(parts[8][:-1]), int(parts[10])


def splits(total, nr):
    """
    >>> splits(3, 3)
    [[0, 0, 3], [0, 1, 2], [0, 2, 1], [0, 3, 0], [1, 0, 2], [1, 1, 1], [1, 2, 0], [2, 0, 1], [2, 1, 0], [3, 0, 0]]
    """
    if nr == 1:
        return [[total]]
    s = []
    for i in range(0, total + 1):
        for split in splits(total - i, nr - 1):
            s.append([i] + split)
    return s


def ingredient_scores(ingredient, teaspoons):
    """
    >>> ingredient_scores((-1, -2, 6, 3), 44)
    [-44, -88, 264, 132]
    >>> ingredient_scores((2, 3, -2, -1), 56)
    [112, 168, -112, -56]
    """
    return list(map(partial(mul, teaspoons), ingredient))


def ingredients_score(ingredients, split):
    """
    >>> ingredients_score([
    ...     (-1, -2, 6, 3),
    ...     (2, 3, -2, -1),
    ... ], [44, 56])
    62842880
    >>> ingredients_score([
    ...     (-1, -2, 6, 3, 8),
    ...     (2, 3, -2, -1, 3),
    ... ], [40, 60])
    57600000
    """
    has_calories = len(ingredients[0]) == 5
    ingredients_property_scores = list(map(
        lambda ingredient_and_teaspoons: ingredient_scores(
            *ingredient_and_teaspoons),
        zip(ingredients, split)))
    property_scores = list(map(sum, zip(*ingredients_property_scores)))
    if has_calories:
        if property_scores[4] != 500:
            return 0
        property_scores = property_scores[:-1]
    if min(property_scores) < 0:
        return 0
    return list(accumulate(property_scores, func=mul))[-1]


def best_ingredients_split(ingredients):
    """
    >>> best_ingredients_split([
    ...     (-1, -2, 6, 3),
    ...     (2, 3, -2, -1),
    ... ])
    62842880
    """
    return max(
        ingredients_score(ingredients, split)
        for split in splits(100, len(ingredients)))


def part1():
    ingredients = list(get_ingredients())
    # Remove the number of calories from the ingredients
    ingredients = [ingredient[:-1] for ingredient in ingredients]
    return best_ingredients_split(ingredients)


def part2():
    return best_ingredients_split(list(get_ingredients()))


if __name__ == '__main__':
    print(part1())
    print(part2())
