#!/usr/bin/env python3
import os
from collections import defaultdict
from functools import partial
from itertools import permutations

path = os.path.join(os.path.dirname(__file__), 'input')


def get_preferences():
    preferences = defaultdict(dict)
    with open(path) as f:
        for row in f.readlines():
            parts = row.split()
            name1 = parts[0]
            name2 = parts[-1].replace('.', '')
            value = -int(parts[3]) if parts[2] == 'lose' else int(parts[3])
            preferences[name1][name2] = value
    return preferences


def placements(people):
    for placement in permutations(people, len(people)):
        yield (*placement, placement[0])


def placement_score(preferences, placement):
    return sum(
        preferences[person1][person2] + preferences[person2][person1]
        for person1, person2
        in zip(placement[:-1], placement[1:]))


def get_best_placement_score(preferences):
    return max(map(
        partial(placement_score, preferences),
        placements(preferences.keys())))


def part1():
    return get_best_placement_score(get_preferences())


def part2():
    preferences = get_preferences()
    for person in dict(preferences):
        preferences['me'][person] = 0
        preferences[person]['me'] = 0
    return get_best_placement_score(preferences)


if __name__ == '__main__':
    print(part1())
    print(part2())
